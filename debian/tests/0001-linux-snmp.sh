#!/bin/bash

set -e

. ./debian/tests/testing-lib

start_snmpd
/usr/lib/nagios/plugins/centreon_plugins.pl \
    --plugin os::linux::snmp::plugin \
    --mode uptime \
    --hostname 127.0.0.1 \
    --snmp-port "${SNMP_PORT}" \
    --snmp-version 2 \
    --verbose
